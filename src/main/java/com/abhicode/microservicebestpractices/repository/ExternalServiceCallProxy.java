package com.abhicode.microservicebestpractices.repository;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "global-exceptional-handler" , url = "localhost:8090")
@RibbonClient(name = "global-exceptional-handler" )
public interface ExternalServiceCallProxy {
    @GetMapping(path = "/welcome")
    public String getWelcomeNote();
}

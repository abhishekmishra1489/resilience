package com.abhicode.microservicebestpractices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.abhicode.microservicebestpractices")
public class MicroservicebestpracticesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicebestpracticesApplication.class, args);
	}

}

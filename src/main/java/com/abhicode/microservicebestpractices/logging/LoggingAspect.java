package com.abhicode.microservicebestpractices.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;


/*
>> 3 dependencies (i)spring-aspects (ii)aspectjweaver (iii)spring-aop
>> Spring can only intercept methods like what to do after the method is executed , or what to print after the error , but cannot intercept at data lavel . Thats where AspectJ comes in picture
>>
*/


@Aspect
@Component
public class LoggingAspect {

    Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut("(within(@org.springframework.stereotype.Component *) || " +
            "within(@org.springframework.stereotype.Service *) || " +
            "within(@org.springframework.web.bind.annotation.RestController *)) && " +
            "within(com.abhicode.microservicebestpractices..*)")

    public void beanAnnotatedWithServiceAnnotation() {
        // just needs this method to be invoked no implementation required
    }

    @Around("beanAnnotatedWithServiceAnnotation()")
    /*ProceedingJoinPoint uses reflection internally . So,  we get the method details and the input parameters that are coming in to the method and can track it .*/

    public Object applicationLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String methodName = signature.getName();
        Object[] objects = joinPoint.getArgs();
        String arguments = Arrays.toString(objects);

        logger.info("Enter: {}.{}() with argument[s] = {}", declaringTypeName,
                methodName, arguments);

        Object result = joinPoint.proceed();
        logger.info("Exit: {}.{}() with result = {}", declaringTypeName,
                methodName, result);
        return result;

    }
}
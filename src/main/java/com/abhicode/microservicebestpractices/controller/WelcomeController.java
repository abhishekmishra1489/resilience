package com.abhicode.microservicebestpractices.controller;

import com.abhicode.microservicebestpractices.exception.BusinessCustomException;
import com.abhicode.microservicebestpractices.service.WelcomeService;
import org.aspectj.weaver.BCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

    private WelcomeService welcomeService ;

    @Autowired
    public WelcomeController(WelcomeService welcomeService) {
        this.welcomeService = welcomeService ;
    }

    @GetMapping("/welcome")
    public String getWelcomeInfo() {
        return "Welcome to microservices best practices" ;
    }

    @GetMapping("/micro2")
    public String getConnectionFromMicro2 () {
       String s = welcomeService.getConnectionFromMicro2();
       return s ;
    }

    @GetMapping("/micro3")
    public String getConnectionFromMicro3 () {
        String s = welcomeService.getConnectionFromMicro2();
        if(!s.startsWith("x")) throw new BusinessCustomException(" GOT THIS ") ;
        return s ;
    }

    @GetMapping("/micro4")
    public String getConnectionFromMicro4 () {
        String s = welcomeService.getConnectionFromMicro3();
        return s ;
    }

}

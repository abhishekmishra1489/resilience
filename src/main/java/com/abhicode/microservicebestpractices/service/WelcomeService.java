package com.abhicode.microservicebestpractices.service;

import com.abhicode.microservicebestpractices.repository.ExternalServiceCallProxy;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WelcomeService {

    @Autowired
    private ExternalServiceCallProxy proxy ;

    Logger logger = LoggerFactory.getLogger(WelcomeService.class);

    private RestTemplate restTemplate ;

    @Autowired
    public WelcomeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @CircuitBreaker(name = "service3", fallbackMethod = "fallbackForMicro2")
    public String getConnectionFromMicro2 () {
        String forObject = restTemplate.getForObject("/welcome", String.class);
        return  forObject ;
    }

    public String fallbackForMicro2 (Throwable t) {
        logger.error("Inside circuit breaker AAABBBHIIII , cause - {}", t.toString());
        String forObject = "fallback for micro2 with Resilience4j";
        return  forObject ;
    }


    public String getConnectionFromMicro3 () {
        String s = proxy.getWelcomeNote();
        logger.info("value from feign : {}" , s);
        return  s ;
    }

}

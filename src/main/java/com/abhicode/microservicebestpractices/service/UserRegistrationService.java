package com.abhicode.microservicebestpractices.service;



import com.abhicode.microservicebestpractices.dto.SellerDto;

import java.util.List;

public interface UserRegistrationService {
    String registerSeller(SellerDto sellerDto);

    List<SellerDto> getSellersList();
}

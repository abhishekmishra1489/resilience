package com.abhicode.microservicebestpractices.exception;

public class BusinessCustomException extends RuntimeException {

    public BusinessCustomException(String msg) {
        super(msg);
    }

}
